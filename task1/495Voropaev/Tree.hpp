﻿#include "common/Mesh.hpp"
#include <vector>
#pragma once

class Tree {
public:
	struct Branch {
		Branch(glm::vec3 a, glm::vec3 b, float ra, float rb)
			: a(a), b(b), ra(ra), rb(rb)
		{}

		// начало и конец
		glm::vec3 a, b;
		// радиус в начале и конце
		float ra, rb;
	};

	Tree(std::vector<Branch>&& tree, int trianglesInBranch = 4)
		: tree(tree), trianglesInBranch(trianglesInBranch), mesh(std::make_shared<Mesh>()) {}

	void Prepare();
	MeshPtr GetMesh();

	bool isPrepared() const { return prepared; }

private:
	// уровень детализации веток
	const int trianglesInBranch;

	std::vector<Branch> tree;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	MeshPtr mesh;

	bool prepared = false;
};

inline MeshPtr Tree::GetMesh() {
	assert(isPrepared());
	return mesh;
}
