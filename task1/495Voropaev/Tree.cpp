﻿#include "Tree.hpp"
#include <vector>

// ветка строится как усеченный конус
void Tree::Prepare() {
	assert(!isPrepared());

	for (const auto& branch : tree) {
		// придаем ветви объем
		const auto ab = branch.b - branch.a;

		auto n1 = glm::normalize(glm::vec3(ab.y, -ab.x, 0));
		if (ab.x == 0 && ab.y == 0) {
			n1 = glm::vec3(1, 0, 0);
		}		
		const auto n2 = glm::normalize(glm::cross(n1, ab));
		auto normal = [&n1, &n2](float phi) {
			return n1 * glm::cos(phi) + n2 * glm::sin(phi);
		};

		for (int i = 0; i < trianglesInBranch; ++i) {
			// углы нижних вершин треугольника в окружности сечения конуса
			float angleStart = 2 * glm::pi<float>() * i / trianglesInBranch;
			float angleEnd = 2 * glm::pi<float>() * (i + 1) / trianglesInBranch;

			//Первый треугольник, образующий квад
			vertices.emplace_back(branch.a + branch.ra * normal(angleStart));
			vertices.emplace_back(branch.a + branch.ra * normal(angleEnd));
			vertices.emplace_back(branch.b + branch.rb * normal(angleEnd));
			normals.emplace_back(normal(angleStart));
			normals.emplace_back(normal(angleEnd));
			normals.emplace_back(normal(angleEnd));

			// Второй
			vertices.emplace_back(branch.b + branch.rb * normal(angleStart));
			vertices.emplace_back(branch.b + branch.rb * normal(angleEnd));
			vertices.emplace_back(branch.a + branch.ra * normal(angleStart));
			normals.emplace_back(normal(angleStart));
			normals.emplace_back(normal(angleEnd));
			normals.emplace_back(normal(angleStart));
		}
	}

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());

	prepared = true;
}
