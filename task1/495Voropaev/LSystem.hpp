﻿#include <vector>
#include <string>
#include <stack>
#include <glm/glm.hpp>
#include "Tree.hpp"
#pragma once

class Grammar {
public:
	// Для упрощения левая часть правил может быть только однобуквенной
	struct Rule {
		Rule(char left, std::string right, float prob = 1)
			:left(left), right(right), prob(prob) {}

		char left;
		std::string right;
		float prob;
	};

	Grammar(std::vector<Rule> rules, std::string start)
		:rules(rules), commands(start) {}

	// сделать шаг грамматики
	std::string ApplyRules(int count = 1) {
		for (int i = 0; i < count; ++i) {
			applyRules();
		}
		return commands;
	}

private:
	std::vector<Rule> rules;
	std::string commands;

	void applyRules();
};

class Plane {
	static const float defaultRotationAngle;
	static const float defaultStartThickness;
	static const float defaultThicknessdecreaseRate;
	static const float defaultLengthdecreaseRate;
	static const glm::vec3 defaultStartPosition;
	static const glm::vec3 defaultStartDirection;
public:
	Plane(glm::vec3 position = defaultStartPosition, glm::vec3 direction = defaultStartDirection,
		float startThickness = defaultStartThickness, float thicknessdecreaseRate = defaultThicknessdecreaseRate,
		float rotationAngle = defaultRotationAngle)
		: state(position, direction, startThickness), thicknessdecreaseRate(thicknessdecreaseRate),
		rotationAngle(rotationAngle), cosAngle(glm::cos(rotationAngle)), sinAngle(glm::sin(rotationAngle)) {}

	Plane(float thickness, float thicknessdecreaseRate, float rotationAngle = defaultRotationAngle)
		: Plane(defaultStartPosition, defaultStartDirection, thickness, thicknessdecreaseRate, rotationAngle) {}

	void Draw(const std::string& commands, std::vector<Tree::Branch>& output);

private:
	struct State {
		State(glm::vec3 position, glm::vec3 direction, float thickness)
			:position(position), direction(direction), thickness(thickness) {}
		glm::vec3 position;
		glm::vec3 direction;
		float thickness;
	};
	State state;
	const float thicknessdecreaseRate;
	const float rotationAngle;
	const float cosAngle;
	const float sinAngle;

	std::stack<State> states;
};
