set(SRC_FILES
    common/Application.cpp
    common/Camera.cpp
    common/DebugOutput.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
    common/Texture.cpp
    LSystem.cpp
    Tree.cpp
    main.cpp
)

set(HEADER_FILES
    common/Application.hpp
    common/Camera.hpp
    common/DebugOutput.h
    common/Mesh.hpp
    common/ShaderProgram.hpp
    common/Texture.hpp
    common/LightInfo.hpp
    LSystem.hpp
    Tree.hpp
)

set(COMMON common)
include_directories(${COMMON})

MAKE_TASK(495Voropaev 1 "${SRC_FILES}" "${HEADER_FILES}")
