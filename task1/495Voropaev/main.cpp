﻿#include <glm/glm.hpp>
#include <vector>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "Tree.hpp"
#include "LSystem.hpp"

std::vector<Tree::Branch> generateTree() {
	std::vector<Tree::Branch> tree;

	std::vector<Grammar::Rule> rules;
	rules.emplace_back('F', "f[&F][^F][<F][>F][F]", 0.8);
	rules.emplace_back('F', "f[&F][^[<F][>F]][F]");
	//rules.emplace_back('F', "f[&+F][^-F][F]");
	Grammar grammar(std::move(rules), "F");
	const auto commands = grammar.ApplyRules(4);

	Plane drawer;
	drawer.Draw(commands, tree);
	return tree;
}

class TreeApplication : public Application {
public:
	MeshPtr _tree;
	ShaderProgramPtr _shader;

	void makeScene() override {
		Tree tree(generateTree());
		tree.Prepare();

		Application::makeScene();
		_tree = tree.GetMesh();
		_tree->setModelMatrix(glm::scale(glm::mat4(1.0f), glm::vec3(0.1f, 0.1f, 0.1f)));

		//=========================================================

		_shader = std::make_shared<ShaderProgram>("495VoropaevData/shader.vert", "495VoropaevData/shader.frag");
	}

	void draw() override {
		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Подключаем шейдер		
		_shader->use();

		//Загружаем на видеокарту значения юниформ-переменных
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		//Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
		_shader->setMat4Uniform("modelMatrix", _tree->modelMatrix());
		_tree->draw();
	}
};

int main() {
	TreeApplication app;
	app.start();

	return 0;
}
