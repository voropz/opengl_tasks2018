﻿#include "Tree.hpp"
#include <vector>
#include <cassert>
#include <random>


void Tree::Prepare() {
	assert(!isPrepared());
	drawBranches();
	prepareMesh(meshB, verticesB, normalsB, texcoordsB);
	prepareMesh(meshL, verticesL, normalsL, texcoordsL);
	prepared = true;
}

// ветка строится как усеченный конус
void Tree::drawBranches() {
	assert(!isPrepared());

	for (const auto& branch : tree) {
		// придаем ветви объем
		const auto ab = branch.b - branch.a;

		auto n1 = glm::normalize(glm::vec3(ab.y, -ab.x, 0));
		if (ab.x == 0 && ab.y == 0) {
			n1 = glm::vec3(1, 0, 0);
		}		
		const auto n2 = glm::normalize(glm::cross(n1, ab));
		auto normal = [&n1, &n2](float phi) {
			return n1 * glm::cos(phi) + n2 * glm::sin(phi);
		};

		for (int i = 0; i < trianglesInBranch; ++i) {
			// углы нижних вершин треугольника в окружности сечения конуса
			float angleStart = 2 * glm::pi<float>() * i / trianglesInBranch;
			float angleEnd = 2 * glm::pi<float>() * (i + 1) / trianglesInBranch;

			//Первый треугольник, образующий квад
			verticesB.emplace_back(branch.a + branch.ra * normal(angleStart));
			verticesB.emplace_back(branch.a + branch.ra * normal(angleEnd));
			verticesB.emplace_back(branch.b + branch.rb * normal(angleEnd));
			normalsB.emplace_back(normal(angleStart));
			normalsB.emplace_back(normal(angleEnd));
			normalsB.emplace_back(normal(angleEnd));
			texcoordsB.emplace_back(0., (i + 0.) / trianglesInBranch);
			texcoordsB.emplace_back(0, (i + 1.) / trianglesInBranch);
			texcoordsB.emplace_back(3, (i + 1.) / trianglesInBranch);

			// Второй
			verticesB.emplace_back(branch.b + branch.rb * normal(angleStart));
			verticesB.emplace_back(branch.b + branch.rb * normal(angleEnd));
			verticesB.emplace_back(branch.a + branch.ra * normal(angleStart));
			normalsB.emplace_back(normal(angleStart));
			normalsB.emplace_back(normal(angleEnd));
			normalsB.emplace_back(normal(angleStart));
			texcoordsB.emplace_back(3, (i + 0.) / trianglesInBranch);
			texcoordsB.emplace_back(3, (i + 1.) / trianglesInBranch);
			texcoordsB.emplace_back(0., (i + 0.) / trianglesInBranch);
		}
		if (branch.leaves) {
			drawLeaves(branch);
		}
	}
}

void Tree::drawLeaves(Branch branch) {
	const auto ab = branch.b - branch.a;
	const auto norm_ab = glm::normalize(ab);

	auto n1 = glm::normalize(glm::vec3(ab.y, -ab.x, 0));
	if (ab.x == 0 && ab.y == 0) {
		n1 = glm::vec3(1, 0, 0);
	}
	const auto n2 = glm::normalize(glm::cross(n1, ab));
	auto normal = [&n1, &n2](float phi) {
		return n1 * glm::cos(phi) + n2 * glm::sin(phi);
	};

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> distr(0.6, 1.05);
	std::uniform_int_distribution<> leaves_on_branch(2, 5);

	for (int i = 0; i < leaves_on_branch(gen); ++i) {
		float pos = distr(gen);
		const auto start = branch.a + (branch.b - branch.a) * pos;

		//Первый треугольник, образующий квад
		verticesL.emplace_back(start - n1 - n2);
		verticesL.emplace_back(start - n1 + n2);
		verticesL.emplace_back(start + n1 + n2);
		normalsL.emplace_back(norm_ab);
		normalsL.emplace_back(norm_ab);
		normalsL.emplace_back(norm_ab);
		texcoordsL.emplace_back(0, 0);
		texcoordsL.emplace_back(0, 1);
		texcoordsL.emplace_back(1, 1);

		// Второй
		verticesL.emplace_back(start - n1 - n2);
		verticesL.emplace_back(start + n1 - n2);
		verticesL.emplace_back(start + n1 + n2);
		normalsL.emplace_back(norm_ab);
		normalsL.emplace_back(norm_ab);
		normalsL.emplace_back(norm_ab);
		texcoordsL.emplace_back(0, 0);
		texcoordsL.emplace_back(1, 0);
		texcoordsL.emplace_back(1, 1);
	}
}

void Tree::prepareMesh(MeshPtr mesh, 
	std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& texcoords) {

	assert(vertices.size() == normals.size());
	assert(vertices.size() == texcoords.size());

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());
}
