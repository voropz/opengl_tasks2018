﻿#include <glm/glm.hpp>
#include <vector>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"
#include "common/LightInfo.hpp"
#include "Tree.hpp"
#include "LSystem.hpp"

std::vector<Tree::Branch> generateTree() {
	std::vector<Tree::Branch> tree;

	std::vector<Grammar::Rule> rules;
	rules.emplace_back('F', "f[&F][^F][<F][>F][F]", 0.8);
	rules.emplace_back('F', "f[&F][^[<F][>F]][F]");
	Grammar grammar(std::move(rules), "F");
	const auto commands = grammar.ApplyRules(4);

	Plane drawer;
	drawer.Draw(commands, tree);
	return tree;
}

class TreeApplication : public Application {
public:
	MeshPtr _tree;
	MeshPtr _leaves;
	ShaderProgramPtr _shader;
	ShaderProgramPtr _shaderL;

	TexturePtr _barkTex;
	TexturePtr _leafTex;
	GLuint _sampler;
	GLuint _samplerL;

	float _lr = 3.0;
	float _phi = 0.0;
	float _theta = 0.48;
	float _attenuation0 = 1.0;
	float _attenuation1 = 0.0;
	float _attenuation2 = 0.05;
	LightInfo _light;

	void makeScene() override {
		Tree tree(generateTree());
		tree.Prepare();

		Application::makeScene();
		_tree = tree.GetMesh().first;
		_tree->setModelMatrix(glm::scale(glm::mat4(1.0f), glm::vec3(0.1f, 0.1f, 0.1f)));

		_leaves = tree.GetMesh().second;
		_leaves->setModelMatrix(glm::scale(glm::mat4(1.0f), glm::vec3(0.1f, 0.1f, 0.1f)));

		//=========================================================

		_shader = std::make_shared<ShaderProgram>("495VoropaevData/forwardLighting.vert", "495VoropaevData/forwardLighting.frag");
		_shaderL = std::make_shared<ShaderProgram>("495VoropaevData/forwardLighting.vert", "495VoropaevData/forwardLighting.frag");
		_barkTex = loadTexture("495VoropaevData/bark3.jpg");
		_leafTex = loadTexture("495VoropaevData/leaves2.png");

		glGenSamplers(1, &_sampler);
		glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_samplerL);
		glSamplerParameteri(_samplerL, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerL, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(_samplerL, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_samplerL, GL_TEXTURE_WRAP_T, GL_REPEAT);

		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		_light.ambient = glm::vec3(0.5, 0.5, 0.5);
		_light.diffuse = glm::vec3(1.0, 1.0, 0.75);
		_light.specular = glm::vec3(1.0, 1.0, 1.0);
		_light.attenuation0 = _attenuation0;
		_light.attenuation1 = _attenuation1;
		_light.attenuation2 = _attenuation2;

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	void draw() override {
		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Подключаем шейдер ствола
		_shader->use();

		//Загружаем на видеокарту значения юниформ-переменных
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		glm::vec3 lightDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
		_shader->setFloatUniform("light.a0", _attenuation0);
		_shader->setFloatUniform("light.a1", _attenuation1);
		_shader->setFloatUniform("light.a2", _attenuation2);
		_shader->setVec3Uniform("light.pos", lightDirCamSpace); //копируем положение уже в системе виртуальной камеры
		_shader->setVec3Uniform("light.La", _light.ambient);
		_shader->setVec3Uniform("light.Ld", _light.diffuse);
		_shader->setVec3Uniform("light.Ls", _light.specular);
		_shader->setFloatUniform("alpha", 1.0);

		glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
		glBindSampler(0, _sampler);
		_barkTex->bind();
		_shader->setIntUniform("diffuseTex", 0);
		
		//Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
		_shader->setMat4Uniform("modelMatrix", _tree->modelMatrix());
		_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _tree->modelMatrix()))));
		_tree->draw();

		// -------
		_shaderL->use();
		_shaderL->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shaderL->setMat4Uniform("projectionMatrix", _camera.projMatrix);
		_shaderL->setFloatUniform("light.a0", _attenuation0);
		_shaderL->setFloatUniform("light.a1", _attenuation1);
		_shaderL->setFloatUniform("light.a2", _attenuation2);
		_shaderL->setVec3Uniform("light.pos", lightDirCamSpace); //копируем положение уже в системе виртуальной камеры
		_shaderL->setVec3Uniform("light.La", _light.ambient);
		_shaderL->setVec3Uniform("light.Ld", _light.diffuse);
		_shaderL->setVec3Uniform("light.Ls", _light.specular);
		_shaderL->setFloatUniform("alpha", 0.8);

		glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
		glBindSampler(0, _samplerL);
		_leafTex->bind();
		_shaderL->setIntUniform("diffuseTex", 0);

		//Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
		_shaderL->setMat4Uniform("modelMatrix", _tree->modelMatrix());
		_shaderL->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _tree->modelMatrix()))));
		_leaves->draw();
		glBindSampler(0, 0);
		glUseProgram(0);

	}
};

int main() {
	TreeApplication app;
	app.start();

	return 0;
}
