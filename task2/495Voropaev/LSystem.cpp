﻿#include "LSystem.hpp"
#include <random>

#include <cassert>

#define GLM_FORCE_RADIANS
const float Plane::defaultRotationAngle = glm::pi<float>() / 7;
const float Plane::defaultStartThickness = 1;
const float Plane::defaultThicknessdecreaseRate = 0.5;
const float Plane::defaultLengthdecreaseRate = 0.75;
const glm::vec3 Plane::defaultStartPosition = glm::vec3(0, 0, -10);
const glm::vec3 Plane::defaultStartDirection = glm::vec3(0, 0, 10);
const int startLeavesFrom = 2;

void Grammar::applyRules() {
	std::random_device rd;
	std::mt19937 gen(rd()); 
	//std::mt19937 gen(42);
	std::uniform_real_distribution<> distr(0, 1);

	std::string output;
	for (const char& letter : commands) {
		bool foundInRules = false;
		for (const auto& rule : rules) {
			if (rule.left == letter && distr(gen) < rule.prob) {
				output += rule.right;
				foundInRules = true;
				break;
			}
		}
		if (!foundInRules) {
			output += letter;
		}
	}
	commands = output;
}


void Plane::Draw(const std::string& commands, std::vector<Tree::Branch>& output) {
	for (const auto& command : commands) {
		switch (command) {
		case 'F':
		case 'f': // в этой системе f = F, просто f не порождает
			output.emplace_back(state.position, state.position + state.direction, state.thickness, state.thickness * thicknessdecreaseRate, state.depth >= startLeavesFrom);
			state.position = state.position + state.direction;
			state.thickness = state.thickness * thicknessdecreaseRate;
			state.direction = state.direction * defaultLengthdecreaseRate;
			state.depth += 1;
			break;
		case '[':
			states.emplace(state);
			break;
		case ']':
			state = states.top();
			states.pop();
			break;
		case '+': // поворот на angle в плоскости oxy
		{
			glm::mat3 rotMatrix(cosAngle, -sinAngle, 0, sinAngle, cosAngle, 0, 0, 0, 1);
			state.direction = rotMatrix * state.direction;
			break;
		}
		case '-': // поворот на -angle в плоскости oxy
		{
			glm::mat3 rotMatrix(cosAngle, sinAngle, 0, -sinAngle, cosAngle, 0, 0, 0, 1);
			state.direction = rotMatrix * state.direction;
			break;
		}
		case '&': // поворот на angle в плоскости oyz
		{
			glm::mat3 rotMatrix(1, 0, 0, 0, cosAngle, -sinAngle, 0, sinAngle, cosAngle);
			state.direction = rotMatrix * state.direction;
			break;
		}
		case '^': // поворот на -angle в плоскости oyz
		{
			glm::mat3 rotMatrix(1, 0, 0, 0, cosAngle, sinAngle, 0, -sinAngle, cosAngle);
			state.direction = rotMatrix * state.direction;
			break;
		}
		case '<': // поворот на angle в плоскости oxz
		{
			glm::mat3 rotMatrix(cosAngle, 0, -sinAngle, 0, 1, 0, sinAngle, 0, cosAngle);
			state.direction = rotMatrix * state.direction;
			break;
		}
		case '>': // поворот на -angle в плоскости oxz
		{
			glm::mat3 rotMatrix(cosAngle, 0, sinAngle, 0, 1, 0, -sinAngle, 0, cosAngle);
			state.direction = rotMatrix * state.direction;
			break;
		}
		default:
			assert(false);
		}
	}
}