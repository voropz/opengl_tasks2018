﻿#include <glm/glm.hpp>
#include <vector>
#include <random>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"
#include "common/LightInfo.hpp"
#include "Tree.hpp"
#include "LSystem.hpp"

std::vector<Tree::Branch> generateTree() {
	std::vector<Tree::Branch> tree;

	std::vector<Grammar::Rule> rules;
	rules.emplace_back('F', "f[&F][^F][<F][>F][F]", 0.8);
	rules.emplace_back('F', "f[&F][^[<F][>F]][F]");
	Grammar grammar(std::move(rules), "F");
	const auto commands = grammar.ApplyRules(4);

	Plane drawer;
	drawer.Draw(commands, tree);
	return tree;
}

class TreeApplication : public Application {
public:
	MeshPtr _tree;
	MeshPtr _leaves;
	MeshPtr _ground;
	ShaderProgramPtr _shader;
	ShaderProgramPtr _cullShader;

	TexturePtr _barkTex;
	TexturePtr _leafTex;
	TexturePtr _groundTex;
	GLuint _sampler;
	GLuint _samplerL;
	GLuint _samplerG;

	float _lr = 10.0;
	float _phi = 0.0;
	float _theta = glm::pi<float>() / 2;
	float _attenuation0 = 1.0;
	float _attenuation1 = 0.0;
	float _attenuation2 = 0.05;
	LightInfo _light;

	std::vector<glm::vec3> _positions;
	DataBufferPtr _bufVec3;
	TexturePtr _bufferTexCulled;
	TexturePtr _bufferTexAll;
	GLuint _TF;
	GLuint _cullVao;
	GLuint _tfOutputVbo;
	GLuint _query;


	void makeScene() override {
		Tree tree(generateTree());
		tree.Prepare();

		Application::makeScene();
		_tree = tree.GetMesh().first;
		_tree->setModelMatrix(glm::scale(glm::mat4(1.0f), glm::vec3(0.1f, 0.1f, 0.1f)));

		_leaves = tree.GetMesh().second;
		_leaves->setModelMatrix(glm::scale(glm::mat4(1.0f), glm::vec3(0.1f, 0.1f, 0.1f)));

		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_real_distribution<> distr(-500, 500);

		for (unsigned int i = 0; i < 1111; i++) {
			_positions.push_back(glm::vec3(distr(gen), distr(gen),0));
		}

		_bufVec3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		_bufVec3->setData(_positions.size() * sizeof(float) * 3, _positions.data());

		glGenBuffers(1, &_tfOutputVbo);
		glBindBuffer(GL_ARRAY_BUFFER, _tfOutputVbo);
		glBufferData(GL_ARRAY_BUFFER, _positions.size() * sizeof(float) * 3, 0, GL_STREAM_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		_bufferTexAll = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
		_bufferTexAll->bind();
		glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, _bufVec3->id());
		_bufferTexAll->unbind();

		_bufferTexCulled = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
		_bufferTexCulled->bind();
		glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, _tfOutputVbo);
		_bufferTexCulled->unbind();


		_ground = makeGroundPlane(500, 500);
		_ground->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, -1.0f)));

		//=========================================================

		_shader = std::make_shared<ShaderProgram>("495VoropaevData/forwardLighting.vert", "495VoropaevData/forwardLighting.frag");

		_cullShader = std::make_shared<ShaderProgram>();
		ShaderPtr vs = std::make_shared<Shader>(GL_VERTEX_SHADER);
		vs->createFromFile("495VoropaevData/cull.vert");
		_cullShader->attachShader(vs);

		ShaderPtr gs = std::make_shared<Shader>(GL_GEOMETRY_SHADER);
		gs->createFromFile("495VoropaevData/cull.geom");
		_cullShader->attachShader(gs);

		_barkTex = loadTexture("495VoropaevData/bark3.jpg");
		_leafTex = loadTexture("495VoropaevData/leaves2.png");
		_groundTex = loadTexture("495VoropaevData/grass2.jpg");

		const char* attribs[] = { "position" };
		glTransformFeedbackVaryings(_cullShader->id(), 1, attribs, GL_SEPARATE_ATTRIBS);
		_cullShader->linkProgram();

		glGenVertexArrays(1, &_cullVao);
		glBindVertexArray(_cullVao);

		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, _bufVec3->id());
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindVertexArray(0);

		glGenTransformFeedbacks(1, &_TF);
		glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _TF);
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, _tfOutputVbo);
		glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

		glGenQueries(1, &_query);

		glGenSamplers(1, &_sampler);
		glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_samplerL);
		glSamplerParameteri(_samplerL, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerL, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(_samplerL, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_samplerL, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glGenSamplers(1, &_samplerG);
		glSamplerParameteri(_samplerG, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerG, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(_samplerG, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_samplerG, GL_TEXTURE_WRAP_T, GL_REPEAT);

		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		_light.ambient = glm::vec3(1.4, 1.4, 1.4);
		_light.diffuse = glm::vec3(1.8, 1.8, 1);
		_light.specular = glm::vec3(3.0, 3.0, 3.0);
		_light.attenuation0 = _attenuation0;
		_light.attenuation1 = _attenuation1;
		_light.attenuation2 = _attenuation2;

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	void cull(const ShaderProgramPtr& shader) {
		shader->use();

		shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);

		glEnable(GL_RASTERIZER_DISCARD);

		glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _TF);

		glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, _query);

		glBeginTransformFeedback(GL_POINTS);

		glBindVertexArray(_cullVao);
		glDrawArrays(GL_POINTS, 0, _positions.size());

		glEndTransformFeedback();

		glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

		glDisable(GL_RASTERIZER_DISCARD);
	}

	void draw() override {
		//Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		//Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		cull(_cullShader);
		
		GLuint primitivesWritten;
		glGetQueryObjectuiv(_query, GL_QUERY_RESULT, &primitivesWritten);

		//Подключаем шейдер
		_shader->use();

		//Загружаем на видеокарту значения юниформ-переменных
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		glm::vec3 lightDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
		_shader->setFloatUniform("light.a0", _attenuation0);
		_shader->setFloatUniform("light.a1", _attenuation1);
		_shader->setFloatUniform("light.a2", _attenuation2);
		_shader->setVec3Uniform("light.pos", lightDirCamSpace); //копируем положение уже в системе виртуальной камеры
		_shader->setVec3Uniform("light.La", _light.ambient);
		_shader->setVec3Uniform("light.Ld", _light.diffuse);
		_shader->setVec3Uniform("light.Ls", _light.specular);
		_shader->setFloatUniform("alpha", 1.0);

		glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
		glBindSampler(0, _samplerG);
		_groundTex->bind();
		//Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
		_shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
		_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));
		_ground->draw();


		glActiveTexture(GL_TEXTURE0);
		glBindSampler(0, _sampler);
		_barkTex->bind();
		_shader->setIntUniform("diffuseTex", 0);

		glActiveTexture(GL_TEXTURE1);
		_bufferTexCulled->bind();
		_shader->setIntUniform("texBuf", 1);
		
		//Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
		_shader->setMat4Uniform("modelMatrix", _tree->modelMatrix());
		_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _tree->modelMatrix()))));
		_tree->drawInstanced(primitivesWritten);

		glActiveTexture(GL_TEXTURE0);
		glBindSampler(0, _sampler);
		_leafTex->bind();
		glActiveTexture(GL_TEXTURE1);
		_bufferTexCulled->bind();
		_shader->setIntUniform("texBuf", 1);
		_leaves->drawInstanced(primitivesWritten);


		glBindSampler(0, 0);
		glUseProgram(0);

	}
};

int main() {
	TreeApplication app;
	app.start();

	return 0;
}
