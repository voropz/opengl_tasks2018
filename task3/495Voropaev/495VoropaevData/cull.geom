#version 430

layout(points) in;
layout(points, max_vertices = 1) out;

uniform mat4 viewMatrix;

out vec3 position;

void main()
{
    //Здесь может быть произвольное условия отбрасывания
    if (length(viewMatrix* vec4(gl_in[0].gl_Position.xyz, 1.0)) < 111.0)
    {
        position = gl_in[0].gl_Position.xyz;
    
        EmitVertex();        
        EndPrimitive();
    }
}
