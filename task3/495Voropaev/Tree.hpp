﻿#include "common/Mesh.hpp"
#include <vector>
#pragma once

class Tree {
public:
	struct Branch {
		Branch(glm::vec3 a, glm::vec3 b, float ra, float rb, bool leaves = false)
			: a(a), b(b), ra(ra), rb(rb), leaves(leaves)
		{}

		// начало и конец
		glm::vec3 a, b;
		// радиус в начале и конце
		float ra, rb;
		bool leaves;
	};

	Tree(std::vector<Branch>&& tree, int trianglesInBranch = 42) : 
		tree(tree), trianglesInBranch(trianglesInBranch), 
		meshB(std::make_shared<Mesh>()), meshL(std::make_shared<Mesh>())
	{}

	void Prepare();
	std::pair<MeshPtr, MeshPtr> GetMesh();

	bool isPrepared() const { return prepared; }

private:
	// уровень детализации веток
	const int trianglesInBranch;

	std::vector<Branch> tree;
	std::vector<glm::vec3> verticesB;
	std::vector<glm::vec3> normalsB;
	std::vector<glm::vec2> texcoordsB;
	std::vector<glm::vec3> verticesL;
	std::vector<glm::vec3> normalsL;
	std::vector<glm::vec2> texcoordsL;
	MeshPtr meshB;
	MeshPtr meshL;

	bool prepared = false;

	void drawBranches();
	void drawLeaves(Branch branch);
	void prepareMesh(MeshPtr mesh,
		std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& texcoords);
};

inline std::pair<MeshPtr, MeshPtr> Tree::GetMesh() {
	assert(isPrepared());
	return { meshB, meshL };
}
